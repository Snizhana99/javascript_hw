## Теоретичні питання

1. Опишіть, як можна створити новий HTML тег на сторінці.
   document.createElement(tag)
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

   targetElement.insertAdjacentHTML(position, text);
   position - визначає позицію елемента котрий додаємо на сторінку, відносно елемента який викливає метод

   Метод elem.insertAdjacentHTML(where, html) вставляє заданий HTML в залежності від значення параметру where:

"beforebegin" – вставляє html прямо перед elem,
"afterbegin" – вставляє html в elem, на початку,
"beforeend" – вставляє html в elem, в кінці,
"afterend" – вставляє html відразу після elem.

ul.insertAdjacentHTML(position, text)

    	"beforebegin"
    		<ul>
    			"afterbegin"
    				<li>1</li>
    				<li>2</li>
    				<li>3</li>
    				<li>4</li>
    				<li>5</li>
    			"beforeend"
    		</ul>
    	"afterend"

3. Як можна видалити елемент зі сторінки?
   Щоб видалити елемент чи вузол зі сторінки вокористовуємо метод WhatRemove.remove()
