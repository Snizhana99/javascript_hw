"use strict";

// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.

// #### Необов'язкове завдання підвищеної складності:

// 1. Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
//    Приклад такого масиву:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// 2. Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let citiesUkraine = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

function showList(arr, parent = document.body) {
  let list = document.createElement("ul");
  parent.append(list);
  arr.map((elem) => {
    if (Array.isArray(elem)) {
      return showList(elem, parent.querySelector("ul"));
    }
    return list.insertAdjacentHTML("beforeend", `<li>${elem}</li>`);
  });
}
showList(citiesUkraine);

let counter = 3;
let div = document.createElement("div");

div.innerText = `The page will be cleared in ${counter}`;
document.body.prepend(div);
div.style.cssText = `color: #c35e9e;
 font-size:26px;
 text-align: center;
 font-weight: 900`;
let timer = setInterval(() => {
  counter--;
  div.innerText = `The page will be cleared in ${counter}`;
}, 1000);

setTimeout(() => {
  clearInterval(timer);
  document.body.innerHTML = "";
}, 3000);
