const priceInput = document.getElementById("price");
priceInput.style.marginTop = "15px";
priceInput.style.color = "blue";

const errorMessage = document.createElement("p");
errorMessage.textContent = "Please enter correct price";
errorMessage.style.color = "red";

priceInput.addEventListener("focus", () => {
  priceInput.style.outlineColor = "green";
});

priceInput.addEventListener("blur", () => {
  priceInput.style.outlineColor = "";
  const price = parseFloat(priceInput.value);
  console.log(price);

  if (price < 0 || isNaN(price)) {
    priceInput.style.borderColor = "red";
    priceInput.insertAdjacentElement("afterend", errorMessage);
  } else {
    errorMessage.remove();
    priceInput.style.borderColor = "";
    const priceDisplay = document.createElement("span");
    priceDisplay.style.outline = "1px solid gray";
    priceDisplay.style.outlineOffset = "2px";
    priceDisplay.style.margin = "15px";

    priceDisplay.textContent = `Current price: ${price}`;
    const closeButton = document.createElement("span");
    closeButton.textContent = "X";
    closeButton.style.cursor = "pointer";
    closeButton.style.border = "1px solid gray";

    closeButton.addEventListener("click", () => {
      priceInput.value = "";
      priceDisplay.remove();
      closeButton.remove();
    });
    priceInput.value = "";
    priceDisplay.appendChild(closeButton);
    priceDisplay.insertAdjacentElement("afterend", closeButton);
    const inputWrap = document.querySelector(".input__wrap");
    inputWrap.insertAdjacentElement("beforebegin", priceDisplay);
  }
});
