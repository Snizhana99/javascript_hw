"use strict";

// ## Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
//   1. При виклику функція повинна запитати дату народження (текст у форматі `dd.mm.yyyy`) і зберегти її в полі `birthday`.
//   2. Створити метод `getAge()` який повертатиме скільки користувачеві років.
//   3. Створити метод `getPassword()`, який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, `Ivan Kravchenko 13.03.1992 → Ikravchenko1992`.
// - Вивести в консоль результат роботи функції `createNewUser()`, а також функцій `getAge()` та `getPassword()` створеного об'єкта.

function createNewUser() {
  const newUser = {
    getLogin() {
      return this.firstName.charAt(0) + this.lastName.toLowerCase();
    },
    getAge() {
      return new Date().getFullYear() - newUser.birthday.slice(-4);
    },
    getPassword() {
      return (
        newUser.firstName[0].toUpperCase() +
        newUser.lastName.toLowerCase() +
        newUser.birthday.slice(-4)
      );
    },
    SetFirstName(newFirstName) {
      Object.defineProperty(newUser, "firstName", {
        value: newFirstName,
      });
    },
    SetLastName(newLastName) {
      Object.defineProperty(newUser, "lastName", {
        value: newLastName,
      });
    },
  };

  //   Object.defineProperties(newUser, {
  //     firstName: {
  //       configurable: true,
  //       writable: false,
  //     },
  //     lastName: {
  //       configurable: true,
  //       writable: false,
  //     },
  //   });
  newUser.firstName = prompt("Please, enter your First name.");
  newUser.lastName = prompt("Please, enter your Last name.");
  newUser.birthday = prompt(
    "Please, enter your date of birthday in the format dd.mm.yyyy"
  );

  //   newUser.firstName = "Snizhana";
  //   newUser.lastName = "Konopatska";
  //   newUser.birthday = "10.08.1999";

  newUser.age = newUser.getAge();

  return newUser;
}

const newUser1 = createNewUser();

console.log(newUser1);

console.log(newUser1.getLogin());
console.log(newUser1.getAge());
console.log(newUser1.getPassword());
