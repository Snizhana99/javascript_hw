function deepClone(obj) {
  if (obj === null || typeof obj !== "object") {
    return obj;
  }
  if (Array.isArray(obj)) {
    return obj.map(deepClone); // массив
  }
  let clone = {};
  for (let prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      clone[prop] = deepClone(obj[prop]);
    }
  }
  return clone;
}

const person = {
  _name: "Uasya",
  "last name": "Pupkin",
  occupation: {
    experience: "1",
    something: null,
  },
  hobby: [{ drawing: 1, reading: 2 }],
  problems: null,
};

const person2 = deepClone(person);
person2._name = "Ivan";
person["last name"] = "Koval";
const newHobby = [{ car: 1, reading: 2 }];
console.log(Array.isArray(newHobby));

person.hobby = newHobby;

console.log(typeof person.hobby);
console.log(typeof newHobby);
console.log(person);
console.log(person2);
