const nextButton = document.querySelector(".next");
const previousButton = document.querySelector(".previous");
const imagesList = document.querySelector(".img__list");
const images = imagesList.querySelectorAll(".img__item");
const imagesLength = images.length;
let curentSlider = 0;

function showNextItem(event) {
  console.log(event.target.closest("button"));

  images[curentSlider].classList.remove("active");

  if (curentSlider < imagesLength - 1) {
    curentSlider++;
  } else {
    curentSlider = 0;
  }

  images[curentSlider].classList.add("active");
  console.log(curentSlider);
}

function showPreviousItem(event) {
  console.log(event.target.closest("button"));

  images[curentSlider].classList.remove("active");

  if (curentSlider > 0) {
    curentSlider--;
  } else {
    curentSlider = imagesLength - 1;
  }
  images[curentSlider].classList.add("active");
  console.log(curentSlider);
}

previousButton.addEventListener("click", showPreviousItem);
nextButton.addEventListener("click", showNextItem);
