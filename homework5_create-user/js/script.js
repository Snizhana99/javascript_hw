"use strict";

// ## Завдання

// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Написати функцію `createNewUser()`, яка буде створювати та повертати об'єкт `newUser`.
// - При виклику функція повинна запитати ім'я та прізвище.
// - Використовуючи дані, введені юзером, створити об'єкт `newUser` з властивостями `firstName` та `lastName`.
// - Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
// - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`. Вивести у консоль результат виконання функції.

// #### Необов'язкове завдання підвищеної складності

//? - Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.
//-----------------------------------------------

// function createNewUser() {
//   const user = {
//     _firstName: prompt("Please, enter your First name."),
//     _lastName: prompt("Please, enter your Last name."),
//     getLogin() {
//       const lowerCaseLogin = (
//         this._firstName.charAt(0) + this._lastName
//       ).toLowerCase();
//       return lowerCaseLogin;
//     },

//     setfirstName(value) {
//       if (typeof value === "string") {
//         this._firstName = value;
//       } else {
//         alert("Error, not string");
//       }
//     },
//     getfirstName() {
//       return this._firstName;
//     },
//   };

//   Object.defineProperty(user, "lastName", {
//     get: function () {
//       return this._lastName;
//     },
//     set: function (value) {
//       this._lastName = value;
//     },
//   });

//   return user;
// }
// let newUser = createNewUser();
// newUser.firstName = "Brendan";
// newUser.lastName = "Eich";
// console.log(newUser);
// console.log(newUser.getLogin(), "(Brendan Eich - напряму не перезаписалися)");
// console.log(newUser);
// console.log(newUser.getLogin());
// newUser.setfirstName(prompt("Please, enter your First name."));
// newUser.getfirstName();
// console.log(newUser.getfirstName());

// newUser._lastName = prompt("Please, enter your Last name.");
// console.log(newUser._lastName);

// ? Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.

//! Чи можна цими двома варіантами досягти виконання цієї умови: заборонити змінити напряму і одочасно змінювати дані властивості?
// setfirstName(value) {
// 	if (typeof value === "string") {
// 	  this._firstName = value;
// 	} else {
// 	  alert("Error, not string");
// 	}
//  },
//  getfirstName() {
// 	return this._firstName;
//  },
// };
//------------------------------
// Object.defineProperty(user, "lastName", {
//  get: function () {
// 	return this._lastName;
//  },
//  set: function (value) {
// 	this._lastName = value;
//  },
// });

//! І не потрібно тоді це? -

//властивості `firstName` та `lastName` не можна було змінювати напряму

// Object.defineProperties(user, {
// 	firstName: {
// 	  writable: false,
// 	},
// 	lastName: {
// 	  writable: false,
// 	},
//  });

function createNewUser() {
  const newUser = {
    firstName: prompt("Please, enter your First name."),
    lastName: prompt("Please, enter your Last name."),
    getLogin() {
      const lowerCaseLogin = (
        this.firstName.charAt(0) + this.lastName
      ).toLowerCase();
      return lowerCaseLogin;
    },
    SetFirstName(newFirstName) {
      Object.defineProperty(newUser, "firstName", {
        value: newFirstName,
      });
    },
    SetLastName(newLastName) {
      Object.defineProperty(newUser, "lastName", {
        value: newLastName,
      });
    },
  };

  Object.defineProperties(newUser, {
    firstName: {
      configurable: true,
      writable: false,
    },
    lastName: {
      configurable: true,
      writable: false,
    },
  });

  return newUser;
}

const newUser = createNewUser();

console.log(newUser);

//>>>>>>>TEST<<<<<<<<<<

// напряму
// newUser.firstName = "Brendan";
// newUser.lastName = "Eich";
console.log(newUser);
console.log(newUser.getLogin(), "(Brendan Eich - напряму не перезаписалися)");

//через метод
newUser.SetFirstName("Napoleon");
newUser.SetLastName("Bonaparte");
console.log(newUser);
console.log(newUser.getLogin());
//-----------------------------------------------------------------
// function createNewUser() {
// 	//   let firstName = prompt("Please, enter your First name.");
// 	//   let lastName = prompt("Please, enter your Last name.");

// 	//   while (firstName === null || lastName === null || !firstName || !lastName) {
// 	//     (firstName = prompt("Please, enter your First name.", firstName)),
// 	//       (lastName = prompt("Please, enter your Last name.", lastName));
// 	//   }
// 	const user = {
// 	  firstName: "Snizhana",
// 	  lastName: "Konopatska",
// 	  getLogin() {
// 		 const lowerCaseLogin = (
// 			this.firstName.charAt(0) + this.lastName
// 		 ).toLowerCase();
// 		 return lowerCaseLogin;
// 	  },
// 	  //  set firstName(newFirstName) {
// 	  //    this.firstName = newFirstName;
// 	  //  },

// 	  //  set lastName(newLastName) {
// 	  //    this.lastName = newLastName;
// 	  //  },
// 	};
// 	Object.defineProperty(user, "firstName", {
// 	  set firstName(newFirstName) {
// 		 this.firstName = newFirstName;
// 	  },
// 	});
// 	Object.defineProperty(user, "lastName", {
// 	  set lastName(newLastName) {
// 		 this.lastName = newLastName;
// 	  },
// 	});
// 	//властивості `firstName` та `lastName` не можна було змінювати напряму
// 	Object.defineProperties(user, {
// 	  firstName: {
// 		 writable: false,
// 	  },

// 	  lastName: {
// 		 writable: false,
// 	  },
// 	});
// 	return user;
//  }
//  let newUser = createNewUser();
//  console.log(newUser);

//  console.log(newUser.getLogin());

//  newUser.newFirstName = "Anna";
//  console.log(newUser);

//  newUser.newLastName = "Open";
//  console.log(newUser);

// const newUser = createNewUser();
// console.log(newUser);

// const newUserLog = newUser.getLogin();
// console.log(newUserLog);

// newUser.setFirstName(prompt("Enter new first name:"));
// console.log(newUser);

// newUser.setLastName(prompt("Enter new last name:"));
// console.log(newUser);

//-----------------------------------------------

// function createNewUser(
//   firstName = prompt("Please, enter your First name."),
//   lastName = prompt("Please, enter your Last name.")
// ) {
//   while (
//     firstName === "null" ||
//     lastName === "null" ||
//     !firstName ||
//     !lastName
//   ) {
//     (firstName = prompt("Please, enter your First name.", firstName)),
//       (lastName = prompt("Please, enter your Last name.", lastName));
//   }

//   {
//     (this.firstName = firstName),
//       (this.lastName = lastName),
//       (this.getLogin = function () {
//         let lowerCase =
//           this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();

//         return lowerCase;
//       });
//   }
// }
// let newUser = new createNewUser();
// console.log(newUser.getLogin());
//------------------------------------

// function createNewUser() {
//   const newUser = {
//     firstName: prompt("Write your first name"),
//     lastName: prompt("Write your last name"),

//     getLogin() {
//       return (this.firstName[0] + this.lastName).toLowerCase();
//     },
//   };
//   return newUser;
// }
// console.log(createNewUser().getLogin());

// function createNewUser() {
//   //   let firstName = prompt("Please, enter your First name.");
//   //   let lastName = prompt("Please, enter your Last name.");

//   while (firstName === null || lastName === null || !firstName || !lastName) {
//     (firstName = prompt("Please, enter your First name.", firstName)),
//       (lastName = prompt("Please, enter your Last name.", lastName));
//   }
//   const newUser = {
//     firstName: prompt("Write your first name"),
//     lastName: prompt("Write your last name"),

//     getLogin() {
//       return (this.firstName[0] + this.lastName).toLowerCase();
//     },
//   };
//   return newUser;
// }
// console.log(createNewUser().getLogin());

// setFirstName(newfirstName) {
//         if (newfirstName) {
//           firstName = newfirstName;
//         }
//       },

// setLastName(newlastName) {
//         if (newlastName) {
//           lastName = newlastName;
//         }
//       },
//------------------------------------
// function createNewUser() {
//   let fName = prompt("Enter your first name:");
//   let lName = prompt("Enter your last name:");

//   while (!fName || !lName || fName === "null" || lName === "null") {
//     fName = prompt("Enter your first name:", fName);
//     lName = prompt("Enter your last name:", lName);
//   }
//   const newUser = {
//     firstName: fName,
//     lastName: lName,
//     getLogin() {
//       const login = (this.firstName[0] + this.lastName).toLowerCase();
//       return `Your login is: ${login}`;
//     },
//     setFirstName(newFirstName) {
//       if (newFirstName) {
//         Object.defineProperty(this, "firstName", { value: newFirstName });
//       }
//     },
//     setLastName(newLastName) {
//       if (newLastName) {
//         Object.defineProperty(this, "lastName", { value: newLastName });
//       }
//     },
//   };

//   Object.defineProperties(newUser, {
//     firstName: {
//       writable: false,
//     },
//     lastName: {
//       writable: false,
//     },
//   });

//   return newUser;
// }

// const user1 = createNewUser();
// console.log(user1);

// const user1Log = user1.getLogin();
// console.log(user1Log);

// user1.setFirstName(prompt("Enter new first name:"));
// console.log(user1);

// user1.setLastName(prompt("Enter new last name:"));
// console.log(user1);
