"use strict";

document.body.addEventListener("keydown", (event) => {
  const buttons = document.querySelectorAll(".btn");
  buttons.forEach((elem) => {
    elem.classList.remove("active");
    if (elem.dataset.key.toLowerCase() === event.key.toLowerCase()) {
      elem.classList.add("active");
    }
  });
});
