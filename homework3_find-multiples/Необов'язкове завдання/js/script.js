"use strict";
// ! #### Необов'язкове завдання підвищеної складності

// - Отримати два числа, `m` і `n`. Вивести в консоль усі прості числа (http://ru.math.wikia.com/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) в діапазоні від `m` до `n` (менше із введених чисел буде `m`, більше буде `n`). Якщо хоча б одне з чисел не відповідає умовам валідації, зазначеним вище, вивести повідомлення про помилку і запитати обидва числа знову.

let m = prompt("Write the number 1");
let n = prompt(
  "Write the number 2 (Number 2 must be bigger than the previous one)"
);
while (
  m === null ||
  n === null ||
  m === "" ||
  n === "" ||
  isNaN(m) ||
  isNaN(n) ||
  !Number.isInteger(+m) ||
  !Number.isInteger(+n)
) {
  m = prompt("Write correct number 1!");
  n = prompt("Write correct number 2!");
}
while (+m >= +n) {
  alert("number 1 is bigger than number 2!");
  m = prompt("Write correct number 1!");
  n = prompt("Write correct number 2!");
}

let num1, num2, i, j;
num1 = +m;
num2 = +n;
for (i = num1; i <= num2; i++) {
  for (j = 2; j <= i; j++) {
    if (i % j === 0 && j < i) {
      break;
    } else if (j === i) {
      console.log(i);
    }
  }
}
