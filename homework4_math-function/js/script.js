"use strict";

// ## Завдання

// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Отримати за допомогою модального вікна браузера два числа.
// - Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено `+`, `-`, `*`, `/`.
// - Створити функцію, в яку передати два значення та операцію.
// - Вивести у консоль результат виконання функції.

// #### Необов'язкове завдання підвищеної складності

// - Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).

let number1;
let number2;
let operation;
do {
  number1 = prompt("Number1?", number1);
} while (isNaN(number1) || number1 === "");
if (number1 !== null) {
  do {
    number2 = prompt("Number2?", number2);
  } while (isNaN(number2) || number2 === "");
}

if (number1 !== null && number2 !== null) {
  do {
    operation = prompt("Operation?");
  } while (!"+-*/".includes(operation));
}
function calcNumbers(number1, number2, operation) {
  switch (operation) {
    case "+":
      return +number1 + +number2;

    case "-":
      return number1 - number2;

    case "/":
      return number1 / number2;

    case "*":
      return number1 * number2;

    default:
      alert("Thanks you for coming"); // Якщо користувач натиснув відміну
  }
}

let result = calcNumbers(number1, number2, operation);
console.log(result);
