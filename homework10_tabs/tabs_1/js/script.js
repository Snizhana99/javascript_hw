"use strict";

let tabs = document.querySelectorAll(".tabs-title");
let contents = document.querySelectorAll(".content");

for (let i = 0; i < tabs.length; i++) {
  tabs[i].addEventListener("click", (event) => {
    //видаляємо активний клас у tab
    let tabsChildren = event.target.parentElement.children;

    for (let t = 0; t < tabsChildren.length; t++) {
      tabsChildren[t].classList.remove("tab-active");
    }
    //додаємо активний клас
    tabs[i].classList.add("tab-active");

    //видаляємо активний клас y content
    let tabContentChildren =
      event.target.parentElement.nextElementSibling.children;
    for (let c = 0; c < tabContentChildren.length; c++) {
      tabContentChildren[c].classList.remove("content-active");
    }
    //додаємо активний клас
    contents[i].classList.add("content-active");
  });
}
