"use strict";

const tabsWrap = document.querySelector(".centered-content");
const tabsTitle = document.querySelectorAll(".tabs-title");
const contents = document.querySelectorAll(".content");

function elementTabs(e) {
  const tab = e.target.dataset.tab;
  if (tab) {
    tabsTitle.forEach((title) => {
      title.classList.remove("active");
    });
    e.target.classList.add("active");

    contents.forEach((content) => {
      content.classList.remove("active");
    });
    const element = document.getElementById(tab);
    element.classList.add("active");
  }
}
tabsWrap.addEventListener("click", elementTabs);
