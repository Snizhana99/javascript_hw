function filterCollection(
  collection,
  keywords,
  allKeywordsRequired,
  ...fields
) {
  const filtered = [];

  collection.forEach((item) => {
    if (
      fields.reduce((acc, field) => {
        const value = field
          .split(".")
          .reduce((obj, key) => (obj ? obj[key] : ""), item);
        if (typeof value === "object") {
          return acc && value !== null && value !== undefined;
        }
        const lowercaseValue =
          typeof value === "string" ? value.toLowerCase() : "";
        const lowercaseKeywords = keywords.toLowerCase().split(" ");
        const matches = lowercaseKeywords.every((keyword) =>
          lowercaseValue.includes(keyword)
        );
        return allKeywordsRequired ? acc && matches : acc || matches;
      }, true)
    ) {
      filtered.push(item);
    }
  });

  return filtered;
}
// Пошук всіх машин, що мають слово 'Toyota' в полі name:
const vehicles = ["Toyota", "May"];
const filteredVehicles = filterCollection(vehicles, "Toyota", true, "name");
console.log(filteredVehicles);

// Пошук усіх статей, що містять слово 'JavaScript' у заголовку або у вмісті:
const articles = ["JavaScript", "JS", "CSS"];
const filteredArticle = filterCollection(
  articles,
  "JavaScript",
  true,
  "title",
  "content"
);
console.log(filteredArticle);

// Пошук усіх статей, які містять хоча б одне з ключових слів 'JavaScript' або 'React' у вмісті:
const filteredArticles = filterCollection(
  articles,
  "JavaScript React",
  false,
  "content"
);
console.log(filteredArticles);

// Пошук усіх елементів, що містять слово 'apple' в полі name, і слово 'green' в полі color:
const filteredItems = filterCollection(
  items,
  "apple green",
  true,
  "name",
  "color"
);
console.log(filteredItems);
