## Теоретичні питання

 <!-- ? 1. Які існують типи даних у Javascript? -->

Оператор typeof - повертає тип даних:

1. string (стрічка)
2. number (число) -підтип числа NaN (не число: 15а, {}, undefined)
3. Symbol
4. BigInt
5. boolean (true, false)
6. undefined (значення не визначено, перевіряє чи присвоєно значення змінні; змінні без значення; неприсвоєне значення)
7. null (пустота)
8. object (-function це object, але може виділятися окремо)

<!-- ? 2. У чому різниця між == і ===? -->

== - дорівнює (перевіряє рівність перетворюючи операнди різних типів до числа)
=== - суворе дорівнює (перевіряє рівність без перетворення типу даних)

<!-- ? 3. Що таке оператор? -->

Оператор - це елемент мови, який задає опис дій які потрібно виконати. Дозволяє отримати значення як результат дії над операндами.
Оператори бувають:
-унарні
-бінарні
-логічні
-присвоєння
-порівняння
-арифметичні
-тернарний оператор
