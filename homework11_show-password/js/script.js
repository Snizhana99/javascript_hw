"use strict";

let input = document.querySelectorAll("#password-input");
let eyeIconsPassword = document.querySelectorAll("#show");

// Зміна іконки та символів
eyeIconsPassword.forEach((eyeIcon) => {
  eyeIcon.addEventListener("click", () => {
    const parInput = eyeIcon.parentElement.querySelector("input");
    if (parInput.type === "password") {
      // eyeIcon.classList.replace("fa-eye-slash", "fa-eye")
      eyeIcon.classList.replace("fa-eye", "fa-eye-slash");
      return (parInput.type = "text");
    }
    // eyeIcon.classList.replace("fa-eye", "fa-eye-slash")
    eyeIcon.classList.replace("fa-eye-slash", "fa-eye");
    parInput.type = "password";
  });
});

// Після натискання на кнопку сторінка не повинна перезавантажуватись
let form = document.querySelector(".password-form");
let button = document.querySelector(".btn");
let redText = document.createElement("p");
let input1 = document.querySelector('input[name="password2"]');

form.addEventListener("submit", function (event) {
  event.preventDefault();

  // перевірка полів вводу

  let password = document.querySelector(".password").value;
  let confirmPassword = document.querySelector(".confirm-password").value;
  if (
    password !== confirmPassword ||
    password.trim() === "" ||
    confirmPassword.trim() === ""
  ) {
    input1.after(redText);
    redText.innerText = "Потрібно ввести однакові значення";
    redText.style.cssText = `color: red;
		 margin-top: -10px;
		 font-size: 12px`;
  } else if (password === confirmPassword) {
    redText.remove();
    alert("You are welcome");
  }
});

// видаляє напис про помилку при фокусу на 2-ий input (прив'язаний текст про помилку до цього input)
input1.addEventListener("focus", function (event) {
  if (input1.nextElementSibling) {
    redText.remove();
  }
});
