// ## Завдання

// Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:
// - Створити порожній об'єкт `student`, з полями `name` та `lastName`.
// - Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// - У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента `tabel`.
// - порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// - Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення `Студенту призначено стипендію`.

let student = {
  firstName: "",
  lastName: "",
  table: {},
};

console.log(student);
while (true) {
  student.firstName = prompt("Please, enter your First name.");
  if (!student.firstName) {
    break;
  }
  student.lastName = prompt("Please, enter your Last name.");
  if (student.lastName) {
    break;
  }
}

let subject, grade;
while (true) {
  subject = prompt("Please, enter subject");
  if (!subject) {
    break;
  }
  grade = +prompt("Please, enter grade");
  while (isNaN(grade)) {
    grade = +prompt("Enter number");
  }

  student.table[subject] = grade;
}

let badGradesCount = 0;
for (let grade in student.table) {
  if (student.table[grade] < 4) {
    badGradesCount++;
  }
}

if (badGradesCount > 0) {
  console.log(`Student has ${badGradesCount} bad grades`);
} else {
  console.log(`The student has been moved to the next course`);
}

let gradesSum = 0;
let gradesCount = 0;
for (let grade in student.table) {
  gradesSum += student.table[grade];
  gradesCount++;
}
let averageGrade = gradesSum / gradesCount;

if (averageGrade > 7) {
  console.log(`The student is awarded a scholarship`);
} else {
  console.log(`The student has not been awarded a scholarship`);
}
