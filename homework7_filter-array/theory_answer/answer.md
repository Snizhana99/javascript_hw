## Теоретичні питання

1.  Опишіть своїми словами як працює метод forEach.

Метод forEach застосовується для перебору масиву, застосовує callback функцію до елементу масиву, але метод нічого не повертає.

2.  Як очистити масив?

let pronouns = ["i", "you", "we", "they", "he", "she", "it"];

console.log(pronouns);

// 1 way
// pronouns = [];
// console.log(pronouns);

// 2 way
// pronouns.length = 0;
// console.log(pronouns);

// 3 way
// pronouns.splice(0, pronouns.length);
// console.log(pronouns);

3.  Як можна перевірити, що та чи інша змінна є масивом?

Array.isArray() - метод перевірки чи масив

Приклад:

let you = [];

console.log(Array.isArray(you));
