"use strict";

// ## Завдання

// Код для завдань лежить в папці project.

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let allTagP = document.getElementsByTagName("p");

for (let changeColor of allTagP) {
  changeColor.style.backgroundColor = "#ff0000";
}
console.log(allTagP);

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let idElem = document.getElementById("optionsList");
console.log(idElem);

console.log(idElem.parentElement);

if (idElem.hasChildNodes()) {
  for (let nameNod of idElem.childNodes) {
    console.log(
      `Name of nod - ${nameNod.nodeName}, type of nod - ${nameNod.nodeType}`
    );
  }
}

// 3. Встановіть в якості контента елемента з id testParagraph наступний параграф - <p>This is a paragraph<p/>
let par = document.getElementById("testParagraph");
par.innerHTML = "<p>This is a paragraph<p/>";
console.log(par);

// 4. Отримати елементи <li>, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let perent = document.querySelector(".main-header");
console.log(perent);
let child = perent.querySelectorAll("li");
console.log(child);
for (let newClass of child) {
  newClass.setAttribute("class", "nav-item");
}
console.log(child);

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let allElemClass = document.querySelectorAll(".section-title");

console.log(allElemClass);
for (let elem of allElemClass) {
  elem.classList.remove("section-title");
}
console.log(allElemClass);
