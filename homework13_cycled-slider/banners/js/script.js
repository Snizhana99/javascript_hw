"use strict";

let slideIndex = 0;

let timer = setInterval(showSlides, 3000);
let i;
let picture = document.querySelectorAll(".image-to-show");

function showSlides() {
  for (i = 0; i < picture.length; i++) {
    picture[i].style.opacity = "0";
  }
  slideIndex++;
  if (slideIndex > picture.length) {
    slideIndex = 1;
  }
  picture[slideIndex - 1].style.opacity = "1";
}
let stopButton = document.querySelector("#stop");
let showAgainButton = document.querySelector("#show-again");
function stop() {
  stopButton.addEventListener("click", () => {
    clearInterval(timer);
    stopButton.disabled = true;
    showAgainButton.disabled = false;
  });
}
function start() {
  showAgainButton.addEventListener("click", () => {
    timer = setInterval(showSlides, 3000);
    stopButton.disabled = false;
    showAgainButton.disabled = true;
  });
}

showSlides();
stop();
start();
