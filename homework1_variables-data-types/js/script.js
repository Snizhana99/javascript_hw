"use strict"
// ## Завдання

// 1) Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
let admin;
const name = "Snizhana";
admin = name;
console.log(admin);

// 2) Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
const days = 10;

const oneDay = 24;
const oneHour = 60;
const oneMinute = 60;

const seconds = days * oneDay * oneHour * oneMinute;
console.log(seconds);


// 3) Запитайте у користувача якесь значення і виведіть його в консоль.

let yourName = prompt("What is your name?");
console.log(yourName);


