"use strict";

let btn = document.querySelector("#theme-button");
let link = document.querySelector("#theme-link");
let activeTheme = localStorage.getItem("theme");

if (activeTheme === "light") {
  link.setAttribute("href", "");
} else if (activeTheme === "dark") {
  link.setAttribute("href", "./css/style-dark_secondary.css");
}

btn.addEventListener("click", () => {
  console.log(link.getAttribute("href"));

  if (link.getAttribute("href")) {
    link.setAttribute("href", "");
    localStorage.setItem("theme", "light");
  } else {
    link.setAttribute("href", "./css/style-dark_secondary.css");
    localStorage.setItem("theme", "dark");
  }
});
